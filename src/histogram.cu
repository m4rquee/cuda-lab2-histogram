#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>

#define COMMENT "Histogram_GPU"
#define RGB_COMPONENT_COLOR 255

typedef struct {
  unsigned char red, green, blue;
} PPMPixel;

typedef struct {
  int x, y;
  PPMPixel *data;
} PPMImage;

double rtclock() {
  struct timezone Tzp;
  struct timeval Tp;
  int stat;
  stat = gettimeofday(&Tp, &Tzp);
  if (stat != 0)
    printf("Error return from gettimeofday: %d", stat);
  return (Tp.tv_sec + Tp.tv_usec * 1.0e-6);
}

// Cuda Kernel Histogram
// No one optimization
__global__ void histograma(int *red, int *green, int *blue, float *h, int n,
                           int qtdBlocos) {
  // int id = blockIdx.x*blockDim.x+threadIdx.x;
  __shared__ int red_ds[64];
  __shared__ int blue_ds[64];
  __shared__ int green_ds[64];

  int j = 0, k = 0, l = 0, count = 0, i, z, t;

  // Find my j, k, l
  l = threadIdx.x % 4;
  k = (threadIdx.x / 4) % 4;
  j = (threadIdx.x / 16) % 4;

  for (i = (blockIdx.x * blockDim.x); i < n; i += blockDim.x * qtdBlocos) {

    if ((threadIdx.x + i) < n) {
      red_ds[threadIdx.x] = red[threadIdx.x + i];
      blue_ds[threadIdx.x] = blue[threadIdx.x + i];
      green_ds[threadIdx.x] = green[threadIdx.x + i];
    }
    __syncthreads();

    // Definir Minimo
    if ((n - i) < 64)
      t = (n - i);
    else
      t = 64;

    for (z = 0; z < t; z++) {
      if (red_ds[z] == j && green_ds[z] == k && blue_ds[z] == l)
        count++;
    }
    __syncthreads();
  }

  float result = (float)count / (float)n;
  atomicAdd(&h[threadIdx.x], result); // Histograma normalizado
}

static PPMImage *readPPM(const char *filename) {
  char buff[16];
  PPMImage *img;
  FILE *fp;
  int c, rgb_comp_color;
  fp = fopen(filename, "rb");
  if (!fp) {
    fprintf(stderr, "Unable to open file '%s'\n", filename);
    exit(1);
  }

  if (!fgets(buff, sizeof(buff), fp)) {
    perror(filename);
    exit(1);
  }

  if (buff[0] != 'P' || buff[1] != '6') {
    fprintf(stderr, "Invalid image format (must be 'P6')\n");
    exit(1);
  }

  img = (PPMImage *)malloc(sizeof(PPMImage));
  if (!img) {
    fprintf(stderr, "Unable to allocate memory\n");
    exit(1);
  }

  c = getc(fp);
  while (c == '#') {
    while (getc(fp) != '\n')
      ;
    c = getc(fp);
  }

  ungetc(c, fp);
  if (fscanf(fp, "%d %d", &img->x, &img->y) != 2) {
    fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
    exit(1);
  }

  if (fscanf(fp, "%d", &rgb_comp_color) != 1) {
    fprintf(stderr, "Invalid rgb component (error loading '%s')\n", filename);
    exit(1);
  }

  if (rgb_comp_color != RGB_COMPONENT_COLOR) {
    fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
    exit(1);
  }

  while (fgetc(fp) != '\n')
    ;
  img->data = (PPMPixel *)malloc(img->x * img->y * sizeof(PPMPixel));

  if (!img) {
    fprintf(stderr, "Unable to allocate memory\n");
    exit(1);
  }

  if (fread(img->data, 3 * img->x, img->y, fp) != img->y) {
    fprintf(stderr, "Error loading image '%s'\n", filename);
    exit(1);
  }

  fclose(fp);
  return img;
}

void Histogram(PPMImage *image, float *h) {

  int i;

  // Device Pixels
  int *h_blue, *h_red, *h_green;

  // Host Pixels
  int *d_blue, *d_red, *d_green;

  // Device result
  float *d_h;

  int n = image->y * image->x;
  // O trabalho será feito em cima destes vetores;
  h_blue = (int *)malloc(n * sizeof(int));
  h_red = (int *)malloc(n * sizeof(int));
  h_green = (int *)malloc(n * sizeof(int));

  /* monta o histograma. */
  for (i = 0; i < n; i++) {
    h_blue[i] = floor((image->data[i].blue * 4) / 256);
    h_green[i] = floor((image->data[i].green * 4) / 256);
    h_red[i] = floor((image->data[i].red * 4) / 256);
  }

  // Alocar Memória no Device
  cudaMalloc(&d_red, n * sizeof(int));
  cudaMalloc(&d_blue, n * sizeof(int));
  cudaMalloc(&d_green, n * sizeof(int));
  cudaMalloc(&d_h, 64 * sizeof(float));

  cudaMemcpy(d_red, h_red, n * sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(d_blue, h_blue, n * sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(d_green, h_green, n * sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(d_h, h, 64 * sizeof(float), cudaMemcpyHostToDevice);

  int qtdBlocos = 128; // 8 SM, 16 blocks per SM; 8*16 = 128
  histograma<<<qtdBlocos, 64>>>(d_red, d_green, d_blue, d_h, n, qtdBlocos);

  cudaMemcpy(h, d_h, 64 * sizeof(float), cudaMemcpyDeviceToHost);

  // Clean Device memory
  cudaFree(d_red);
  cudaFree(d_blue);
  cudaFree(d_green);
  cudaFree(d_h);
}

int main(int argc, char *argv[]) {

  if (argc != 2) {
    printf("Too many or no one arguments supplied.\n");
  }

  double t_start, t_end;
  int i;
  char *filename = argv[1]; // Recebendo o arquivo!;

  // scanf("%s", filename);
  PPMImage *image = readPPM(filename);

  float *h = (float *)malloc(sizeof(float) * 64);

  // Inicializar h
  for (i = 0; i < 64; i++)
    h[i] = 0.0;

  t_start = rtclock();
  Histogram(image, h);
  t_end = rtclock();

  for (i = 0; i < 64; i++) {
    printf("%0.3f ", h[i]);
  }
  printf("\n");
  fprintf(stdout, "%0.6lf", t_end - t_start);
  free(h);
}
